import re

def parseForumPath(path):
    p = re.compile('^/f([0-9]+)-.*$')
    m = p.match(path)
    if len(m.groups()) != 1 or not m.groups()[0].isdigit():
        return -1

    return int(m.groups()[0])

class Forum:
    def __init__(self):
        self.id = -1
        self.title = ''
        self.description = ''
        self.parent = -1

    def save(self, cur):
        if self.id == -1:
            print('Error: Forum has no ID!')
            return
        cur.execute('INSERT INTO forums(id, title, description, parent) VALUES(?, ?, ?, ?)',
                        (self.id, self.title, self.description, self.parent))

    def __str__(self):
        return 'Title: ' + self.title + '\nDescription: ' + self.description + '\nParent: ' + str(self.parent)