from bs4 import BeautifulSoup
from bs4 import NavigableString

import webutils
from post import Post
import forum

SMILIES = {
    'Very Happy': ':D',
    'Smile': ':)',
    'Sad': ':(',
    'Marrant': '^^',
    'Surprised': ':o',
    'Shocked': 'O.O',
    'Cool': '8)',
    'Laughing': ':lol:',
    'Mad': ':x',
    'Razz': ':P',
    'Embarassed': ':oops:',
    'Crying or Very sad': ':\'(',
    'Evil or Very Mad': ':evil:',
    'Twisted Evil': ':twisted:',
    'Rolling Eyes': ':roll:',
    'Wink': ';)',
    'Exclamation': ':!:',
    'Question': ':?:',
    'Arrow': ':arrow:',
    'Neutral': ':|',
    'Suspect': ':suspect:',
    'I love you': ':heart:',
    'No': ':no:',
    'cyclops': ':cyclops:',
    'pirat': ':pirat:',
    'tongue': ':p',
    'silent': ':silent:',
    'pale': ':pale:',
    'Smiley qui rit': 'xD',
    'bounce': ':bounce',
    'Tête de con': ':hap:',
    'confused': ':confused:',
    'affraid': ':affraid',
    'Basketball': ':bball:',
    'cheers': ':cheers',
    'drunken': ':drunken:',
    'Sleep': ':sleep:',
    'sunny': ':sunny:',
    'king': ':king:',
    'scratch': ':scratch:',
    'study': ':study:',
    'Mort de rire': ':bolox:',
    'Pisse de rire': ':hahaha:',
    'Secoué': ':nuclear-bomb:',
    'flower': ':flower:',
    'lol!': ':lol!:',
    'Attend patiemment': ':bed:',
    'Mexicain très fâché': ':mcrft-angry:',
    'Mexicain amusé': ':mncft-funny:',
    'Mexicain souriant ge': ':mncrft-smile:',
    'Bon anniversaire !': ':mncrft-cake:',
    'Mexicain envieux': ':bave:',
    'Mexicain qui se la p': ':mncrft-cool:',
    'Mexicain maladif': ':mncrft-malade:',
    'Mexicain attristé': ':mncrft-sad:',
    'Mexicain désespéré': ':mncrft-despera:',
    'Oh !': ':mncrft-eyes:',
    'À mort le mexicain !': ':mncrft-todeath:',
    'Down': ':down:',
    'Vive les bookshelfs': ':mncrft-diction:',
    'Déjà proposé, déjà r': ':deja-refuse:',
    #'+1', ':fb:', # Already taken by +1 below, plus it's a facebook icon??
    'Troll': ':troll:',
    'Editer': ':éditer:',
    '+1': '+1',
    '-1': '-1',
    '🤡': ':clown:'
}


def find_additional_pages(soup):
    """Returns a list of post numbers corresponding to additional pages. `soup` must be the .pagination <p>."""

    pages = []
    for page in soup.select('strong'):
        pages.append(page.string)

    if len(pages) != 2:
        print('WARNING: Could not parse pagination')
        print(soup)
        return

    if pages[0] != '1':
        print('WARNING: First page is not 1!')
        print(soup)
        return

    additional_pages = []
    for i in range(1, int(pages[1])):
        additional_pages.append(str(25 * i))

    return additional_pages

class Thread:

    def __init__(self, domain_name, html, thread_number):
        self.posts = []
        self.domain_name = domain_name
        self.id = thread_number

        soup = BeautifulSoup(html, 'html.parser')

        posts = soup.select('.post')
        if len(posts) == 0:
            self.found = False
            self.title = None
            self.content = None
            return
        self.found = True

        self.title = soup.select_one('.page-title a').string
        if self.title == None:
            print('Error: Could not fetch post title')
            self.found = False
            return

        self.additional_pages = find_additional_pages(soup.select_one('.pagination'))

        forum_path = soup.select('.topic-actions .pathname-box a')
        if forum_path is None or not forum_path or not forum_path[len(forum_path) - 1].has_attr('href'):
            print('Error: Could not get forum path')
            self.forum = -1
        else:
            self.forum = forum.parseForumPath(forum_path[len(forum_path) - 1]['href'])

        self.add_posts(soup)

    def __soup_to_markdown(self, soup):
        md = ''

        if soup.name is None:
            md = soup.replace('*', '\\*') \
                    .replace('(', '\\(') \
                    .replace(')', '\\)') \
                    .replace('[', '\\[') \
                    .replace(']', '\\]') \
                    .replace('~', '\\~') \
                    .replace('|', '\\|')
        elif soup.name == 'strong':
            contents = ''
            for child in soup.children:
                contents = contents + self.__soup_to_markdown(child)

            md = '**' + contents + '**'
        elif soup.name == 'i':
            contents = ''
            for child in soup.children:
                contents = contents + self.__soup_to_markdown(child)

            md = '*' + contents + '*'
        elif soup.name == 'strike':
            contents = ''
            for child in soup.children:
                contents = contents + self.__soup_to_markdown(child)

            md = '~~' + contents + '~~'
        elif soup.name == 'a':
            contents = ''
            for child in soup.children:
                contents = contents + self.__soup_to_markdown(child)

            if soup.has_attr('href'):
                md = '[' + contents + '](' + soup['href'] + ')'
            else:
                md = '[' + contents + '](#)'
        elif soup.name == 'br':
            md = '  \n'
        elif soup.name == 'font' or soup.name == 'div' or soup.name == 'span' or soup.name == 'cite' or soup.name == 'u': #Ignored by calling this function recursively on the tag's contents
            for bit in soup.children:
                md = md + self.__soup_to_markdown(bit)
        elif soup.name == 'img':
            if soup.has_attr('alt') and soup.has_attr('src') and soup['alt'] is None:
                print('WARNING: Image without alt: ' + str(soup) + ' (alt: ' + soup['alt'] + ')')
            elif soup.has_attr('alt') and soup.has_attr('src') and soup['alt'] in SMILIES:
                md = SMILIES[soup['alt']]
            elif soup.has_attr('alt') and soup.has_attr('src') and soup['alt'] == '':
                localImage = webutils.downloadImage(self.domain_name, soup['src'])
                if localImage == None:
                    md = '![User-generated image](' + soup['src'] + ')'
                else:
                    md = '![User-generated image](' + localImage + ')'
            elif soup.has_attr('alt') and soup.has_attr('src'):
                md = '![' + soup['alt'] + '](' + soup['src'] + ')'
                print('WARNING: Unknown alt tag: ' + soup['alt'])
            elif soup.has_attr('src'):
                md = '![](' + soup['src'] + ')'
                print('WARNING: No alt tag on img')
            else:
                print('WARNING: No alt tag and no src tag on img')
        elif soup.name == 'blockquote':
            md = md + '\n'
            for content in soup.children:
                md = md + '\n> ' + self.__soup_to_markdown(content)
        elif soup.has_attr('class') and 'spoiler' in soup['class']: # Spoiler
            spoiler_contents = soup.select_one('.spoiler_content')
            md = self.__soup_to_markdown(spoiler_contents)
        elif soup.name == 'ul':
            md = md + '\n'
            for li in soup.children:
                contents = ''
                for content in li.children:
                    contents = contents + self.__soup_to_markdown(content)

                md = md + '* ' + contents + '\n'
       #elif soup.name == 'table':
       #    for row in soup.contents[0].children:
       #        md = md + '\n| '
       #        for cell in row.children:
       #            if not isinstance(cell, NavigableString):
       #                for child in cell.children:
       #                    md = md + self.__soup_to_markdown(child)
       #            else:
       #                md = md + str(cell)
       #            md = md + ' | '
        else:
            print('WARNING: Unknown tag ' + soup.name)

        return md

    def add_posts_html(self, html):
        soup = BeautifulSoup(html, 'html.parser')
        self.add_posts(soup)

    def add_posts(self, soup):
        posts = soup.select('.post')

        for p in posts:
            if not p.has_attr('id') or p['id'] == 'p0' or p['id'] == 'ptrafic_close' or p['id'] == 'ptrafic_open':
                continue

            print('Parsing post ' + p['id'])

            post = Post()

            post.title = p.select_one('.topic-title a').string

            has_author = True
            author = p.select_one('.postbody .author a')
            if author != None and author.has_attr('href'): # If the account has been deleted, it will appear as 'Guest' with no link
                post.author = author['href'][2:]
            else:
                post.author = '-1'
                has_author = False
            if has_author:
                post.date = p.select_one('.postbody .author').contents[3]
            else:
                post.date = p.select_one('.postbody .author').contents[1]

            for bit in p.select_one('.inner .postbody .content div').children:
                post.content = post.content + self.__soup_to_markdown(bit)

            self.posts.append(post)

    def save(self, conn):
        """Save all the messages to the DB and creates a corresponding thread"""

        if not self.posts:
            return

        cur = conn.cursor()
        cur.execute('''
            INSERT INTO threads(id, first_message, forum) VALUES(?, ?, ?)
        ''', (self.id, -1, self.forum))

        first_message = -1
        thread = cur.lastrowid

        for post in self.posts:
            if first_message == -1:
                first_message = post.save(cur.lastrowid, cur)
            else:
                post.save(thread, cur)

        cur.execute('''
            UPDATE threads SET first_message=? WHERE id=?
        ''', (first_message, thread))

        conn.commit()

    def __str__(self):
        out = ''
        for post in self.posts:
            out = out + str(post) + '\n\n~~~~~~~~~~~~\n\n'

        return out
