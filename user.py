from bs4 import BeautifulSoup
from bs4 import NavigableString
import re

import webutils

class User:
    def __init__(self, domain_name, html, user_number):

        soup = BeautifulSoup(html, 'html.parser')
        name = soup.select_one('.page-title strong') # Moderators have a strong tag
        if name == None: # Not a moderator
            name = soup.select_one('.page-title')
            if name == None:
                self.found = False
                return
            
            if soup.select_one('#cp-main') is None: # user doesn't exist (can't use return code because it is still 200...)
                self.found = False
                return
            
            p = re.compile('^.*\\: (.*)$')
            m = p.match(name.string)
            if len(m.groups()) != 1:
                print("Error parsing username")
                self.found = False
                return
            self.name = m.groups()[0]
        else:
            self.name = name.string
        
        self.found = True
        self.id = user_number
        
        joined = soup.select_one('#field_id-4 .field_uneditable')
        if joined is None:
            self.joined = ''
        else:
            self.joined = joined.string
        birth = soup.select_one('#field_id-12 .field_uneditable')

        if birth is None:
            self.birth = ''
        else:
            self.birth = birth.string

        gender = soup.select_one('#field_id-7 .field_uneditable img')
        if gender is None or not gender.has_attr('alt'):
            self.gender = ''
        else:
            if gender['alt'] == 'Masculin': # Will depend on the locale presumably
                self.gender = 'Male'
            else:
                self.gender = 'Female'

        location = soup.select_one('#field_id-11 .field_uneditable')
        if location is None:
            self.location = ''
        else:
            self.location = location.string
        
        avatar = soup.select_one('#profile-advanced-right .module .inner div img')
        if avatar is None or not avatar.has_attr('src'):
            self.avatar = ''
        else:
            self.avatar = webutils.downloadImage(domain_name, avatar['src'])

    def save(self, conn):
        """Save the user to the DB and creates a corresponding thread"""

        if not self.found:
            return

        cur = conn.cursor()
        cur.execute('''
            INSERT INTO users(id, name, gender, birth, joined, location, avatar) VALUES(?, ?, ?, ?, ?, ?, ?)
        ''', (self.id, self.name, self.gender, self.birth, self.joined, self.location, self.avatar))
        conn.commit()

    def __str__(self):
        return '#' + self.id + ' (' + self.name + '): gender=' + self.gender + ', birth=' + self.birth + ', joined=' + self.joined + ', location=' + self.location + ', avatar=' + self.avatar
