#!/usr/bin/env python3

import sys
import os
import os.path
import re
import sqlite3
import time

from thread import Thread
from user import User
import webutils
from forumList import ForumList

def main():
    if len(sys.argv) != 5:
        print('Usage: ./phpbb-scrapper.py <last thread number> <last user number> <wait time> <domain>')
        exit()

    first_thread_number = 1
    first_user_number = 1
    last_thread_number = int(sys.argv[1])
    last_user_number = int(sys.argv[2])
    wait_time = sys.argv[3]
    domain_name = sys.argv[4]

    if not validDomainName(domain_name):
        print('Error: Invalid domain name: ' + domain_name + '. Make sure to only include the domain, without any slashes, protocols or file names!')
        exit()

    if not wait_time.isdigit():
        print('Error: invalid wait time')
        exit()
    wait_time = int(wait_time)

    conn = None
    if os.path.isfile(domain_name + '.sqlite'):
        conf = input('Database already exists (' + domain_name + '.sqlite). Attempt to recover? (Y/n) ')
        recover = True
        if conf != 'Y' and conf != '':
            recover = False

        if not recover:
            os.remove(domain_name + '.sqlite')
            conn = sqlite3.connect(domain_name + '.sqlite')
            create_database(conn, domain_name)
        else:
            conn = sqlite3.connect(domain_name + '.sqlite')
            cur = conn.cursor()

            cur.execute('SELECT MAX(id) FROM threads')
            for row in cur:
                if row[0] == None:
                    first_thread_number = 1
                else:
                    first_thread_number = row[0] + 1

            cur.execute('SELECT MAX(id) FROM users')
            for row in cur:
                if row[0] == None:
                    first_user_number = 1
                else:
                    first_user_number = row[0] + 1

    else:
        conn = sqlite3.connect(domain_name + '.sqlite')
        create_database(conn, domain_name)

    conf = input('Iterating from thread ' + str(first_thread_number) + ' to thread ' + str(last_thread_number) + ', and from user ' + str(first_user_number) + ' to user ' + str(last_user_number) + ' (Y/n) ')
    if conf != 'Y' and conf != '':
        print('Could not confirm, exiting...')
        exit()

    download_threads(conn, domain_name, wait_time, first_thread_number, last_thread_number)
    download_users(conn, domain_name, wait_time, first_user_number, last_user_number)

    conn.close()

def download_forums(conn, domain_name):
    url = 'http://' + domain_name
    html = webutils.get(url)
    forumList = ForumList(domain_name, html)
    forumList.save(conn)

def download_threads(conn, domain_name, wait_time, first_thread_number, last_thread_number):
    for thread_number in range(first_thread_number, last_thread_number + 1):
        url = 'http://' + domain_name + '/t' + str(thread_number) + '-'
        html = webutils.get(url)
        thread = Thread(domain_name, html, thread_number)
        if thread.found:
            for page in thread.additional_pages:
                url = 'http://' + domain_name + '/t' + str(thread_number) + 'p' + page + '-'
                html = webutils.get(url)
                thread.add_posts_html(html)

            thread.save(conn)

        time.sleep(wait_time)

def download_users(conn, domain_name, wait_time, first_user_number, last_user_number):
    for user_number in range(first_user_number, last_user_number + 1):
        url = 'http://' + domain_name + '/u' + str(user_number)
        html = webutils.get(url)
        user = User(domain_name, html, user_number)
        if user.found:
            user.save(conn)

        time.sleep(wait_time)

def validDomainName(domain_name):
    p = re.compile('^[A-Za-z.0-0\\-]+$')
    if p.match(domain_name).span() != (0, len(domain_name)):
        return False
    return True

def create_database(conn, domain_name):
    cur = conn.cursor()
    cur.execute('''
        CREATE TABLE users(id INTEGER PRIMARY KEY, name TEXT, gender INTEGER, birth TEXT, joined TEXT, location TEXT, avatar BLOB)
    ''')
    cur.execute('''
        CREATE TABLE forums(id INTEGER PRIMARY KEY, title TEXT, description TEXT, parent INTEGER)
    ''')
    cur.execute('''
        CREATE TABLE messages(id INTEGER PRIMARY KEY, thread INTEGER, title TEXT, author INTEGER, content TEXT, date TEXT)
    ''')
    cur.execute('''
        CREATE TABLE threads(id INTEGER PRIMARY KEY, first_message INTEGER, forum INTEGER)
    ''')
    conn.commit()

    download_forums(conn, domain_name)

main()
